/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.manager.impl;

import org.osgi.framework.*;
import thinlet.FrameLauncher;

import java.util.Locale;

/**
 * Provides the bundle activator for the manager bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator implements BundleActivator {

  private static BundleContext c_BundleContext;
  private ManagerDesktop m_ManagerDesktop;
  private FrameLauncher m_FrameLauncher;
  private static ServiceMediator c_Services;


  public void start(BundleContext bundleContext)
      throws Exception {
    c_BundleContext = bundleContext;
    c_Services = new ServiceMediator();
    c_Services.activate(c_BundleContext);

    m_ManagerDesktop = new ManagerDesktopImpl();
    m_ManagerDesktop.activate();
    //prepareDefinitions thinlet
    m_FrameLauncher = new FrameLauncher("Coalevo Management Desktop", (ManagerDesktopImpl) m_ManagerDesktop, 640, 480);
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {
    m_ManagerDesktop.deactivate();
    m_FrameLauncher.dispose();
    c_Services.deactivate();
    m_FrameLauncher = null;
    m_ManagerDesktop = null;
    c_BundleContext = null;
    c_Services = null;
  }//stop


  public static BundleContext getBundleContext() {
    return c_BundleContext;
  }//getBundleContext

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Bundle findBundle(String pid) {
    final ServiceReference sr =
        c_BundleContext.getServiceReference(pid);

    if (sr == null) {
      //System.out.println("findBundle() =====>  sr==null");
      ServiceReference[] srefs = null;
      //try to find a ManagedService
      //System.out.println("findBundle() =====> Searching for" +
      //      "(" + Constants.SERVICE_PID + "=" + pid + ")");
      try {
        srefs = c_BundleContext.getAllServiceReferences(
            "org.osgi.service.cm.ManagedService",
            "(" + Constants.SERVICE_PID + "=" + pid + ")"
        );
      } catch (Exception ex) {
        //System.out.println("findBundle() =====>  " + ex.getMessage());
        //ex.printStackTrace(System.out);
      }
      if (srefs == null || srefs.length == 0) {
        //System.out.println("findBundle() =====>  srefs null or empty atempting factory");
        try {
          //try to find managed factories with pid as filter
          srefs = c_BundleContext.getServiceReferences(
              "org.osgi.service.cm.ManagedServiceFactory",
              "(" + Constants.SERVICE_PID + "=" + pid + ")");
        } catch (Exception ex) {
          //System.out.println("findBundle() =====>  " + ex.getMessage());
          //ex.printStackTrace(System.out);
          return null;
        }
      }
      if (srefs == null || srefs.length == 0) {
        //System.out.println("findBundle() =====>  srefs still null or empty" );
        return null;
      } else {
        return srefs[0].getBundle();
      }
    }
    //System.out.println("findBundle() =====>  " + sr.getBundle());

    return sr.getBundle();
  }//findBundle

  public static String localeToString(Locale l) {
    final String lang = l.getLanguage();
    final String cntry = l.getCountry();
    final String var = l.getVariant();

    final StringBuffer sbuf = new StringBuffer();
    if (lang != null && lang.length() > 0) {
      sbuf.append(lang);
      if (cntry != null && cntry.length() > 0) {
        sbuf.append(LOCSEP);
        sbuf.append(cntry);
        if (var != null && var.length() > 0) {
          sbuf.append(LOCSEP);
          sbuf.append(var);
        }
      }
    }

    return sbuf.toString().toLowerCase();
  }//localeToString


  private static final String LOCSEP = "_";

}//class Activator
