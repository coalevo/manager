/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.manager.impl;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.security.model.*;
import net.coalevo.security.service.PolicyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thinlet.Thinlet;

import java.io.IOException;
import java.util.*;

/**
 * Provides a {@link Handler} implementation that will handle the UI
 * of the policy panel.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PolicyHandler
    implements Handler {

  private static final Logger log = LoggerFactory.getLogger(PolicyHandler.class);
  private ResourceBundle m_Resources = ResourceBundle.getBundle("net.coalevo.manager.resources.string");

  private ManagerDesktopImpl m_Thinlet;
  private UserAgent m_Agent;
  private ServiceMediator m_Services;
  private Object m_PolicyDialog;
  private Policy m_EditPolicy;
  private Object m_PolicyEntryDialog;


  public PolicyHandler(Thinlet thinlet) {
    m_Thinlet = (ManagerDesktopImpl) thinlet;
  }//constructor

  public void init() {
    m_Services = m_Thinlet.getServices();
    m_Agent = m_Thinlet.getUserAgent();
    preparePolicies();
  }//init

  public void deinit() {
  }//deinit

  public void actionCreatePolicy() {
    final Object identifier = m_Thinlet.find(m_Thinlet.getActualPanel(), "policy.identifier");
    String id = m_Thinlet.getString(identifier, "text");
    if (id == null || id.length() < 3) {
      m_Thinlet.showError(m_Resources.getString("policy.nameinvalid") + " [" + id + "].");
      return;
    } else {
      Policy p = m_Services.getPolicyService().createPolicy(m_Agent, id);
      if (p == null) {
        m_Thinlet.showError(m_Resources.getString("policy.createfailed") + " [" + id + "].");
      } else {
        addItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "policies.list"), id);
        m_Thinlet.setString(identifier, "text", "");
      }
    }
    m_Thinlet.updateSession();
  }//actionCreatePolicy

  public void actionDeletePolicy() {
    Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "policies.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      String id = m_Thinlet.getString(item, "text");
      //TODO: catch IllegalState and report correctly
      if (m_Services.getPolicyService().deletePolicy(m_Agent, id)) {
        m_Thinlet.remove(item);
      } else {
        m_Thinlet.showError(m_Resources.getString("policy.removefailed") + " [" + id + "].");
      }
    }
    m_Thinlet.updateSession();
  }//actionDeletePolicy

  public void actionViewPolicy() {
    final PolicyService ps = m_Services.getPolicyService();
    final Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "policies.list");
    final Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      final String id = m_Thinlet.getString(item, "text");
      Policy p = null;
      String policy = "";
      try {
        p = ps.leasePolicy(m_Agent, id);
        policy = m_Services.getPolicyXMLService().toXML(p);
      } catch (IOException ex) {
        log.error("actionViewPolicy()", ex);
      } finally {
        ps.releasePolicy(p);
      }
      Object view = m_Thinlet.addComponent(m_Thinlet, "/net/coalevo/manager/resources/policy_view.xml", this);
      //if (view == null) System.err.println("view null!");
      m_Thinlet.setString(view, "text", id);
      //if (m_Thinlet.find(view, "policyxml") == null) System.err.println("policyxml null!");

      m_Thinlet.setString(m_Thinlet.find(view, "policyxml"), "text", policy);
    }
    m_Thinlet.updateSession();
  }//actionViewPolicy

  public void actionViewPolicyDone(Object dialog) {
    log.debug("actionCiewPolicyDone()");
    m_Thinlet.remove(dialog);
    m_Thinlet.updateSession();
  }//actionViewPolicyDone


  public void actionEditPolicy() {
    final PolicyService ps = m_Services.getPolicyService();

    //1. get selected
    Object item = m_Thinlet.getSelectedItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "policies.list"));
    if (item == null) {
      return;
    }
    final String id = m_Thinlet.getString(item, "text");
    m_EditPolicy = ps.leasePolicy(m_Agent, id);
    m_PolicyDialog = m_Thinlet.addComponent(m_Thinlet, "/net/coalevo/manager/resources/policy_dialog.xml", this);
    m_Thinlet.setString(m_PolicyDialog, "text", id);
    prepareEntries();
    m_Thinlet.updateSession();
  }//actionEditPolicy

  public void actionEditPolicyDone() {
    log.debug("actionEditPolicyDone()");
    m_Thinlet.remove(m_PolicyDialog);
    m_PolicyDialog = null;
    //release the edit policy
    m_Services.getPolicyService().releasePolicy(m_EditPolicy);
    m_EditPolicy = null;

    m_Thinlet.updateSession();
  }//actionEditPolicyDone


  public void actionAddPolicyEntry() {
    final PolicyService ps = m_Services.getPolicyService();
    final Object identifier = m_Thinlet.find(m_PolicyDialog, "action.identifier");
    String id = m_Thinlet.getString(identifier, "text");
    if (id == null || id.length() < 3) {
      m_Thinlet.showError(m_Resources.getString("policy.entry.nameinvalid") + " [" + id + "].");
      return;
    } else {
      Action a = new Action(id);
      if (!ps.putPolicyEntry(m_Agent, m_EditPolicy, a, ps.getBaseAuthorizationRule())) {
        m_Thinlet.showError(m_Resources.getString("policy.entry.addfailed") + " [" + id + "].");
      } else {
        Object list = m_Thinlet.find(m_PolicyDialog, "entries.list");
        Object item = addItem(list, a);
        m_Thinlet.setBoolean(item, "selected", true);
        actionEditPolicyEntry();
      }
    }
  }//actionAddEntry

  public void actionRemovePolicyEntry() {
    final PolicyService ps = m_Services.getPolicyService();
    Object list = m_Thinlet.find(m_PolicyDialog, "entries.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      Action a = (Action) m_Thinlet.getProperty(item, "action");
      if (ps.removePolicyEntry(m_Agent, m_EditPolicy, a)) {
        m_Thinlet.remove(item);
      } else {
        m_Thinlet.showError(m_Resources.getString("policy.entry.removefailed") + " [" + a.getIdentifier() + "].");
      }
    }
    m_Thinlet.updateSession();
  }//actionRemoveEntry


  public void actionEditPolicyEntry() {
    //1. get selected
    Object list = m_Thinlet.find(m_PolicyDialog, "entries.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    }
    final Action a = (Action) m_Thinlet.getProperty(item, "action");
    m_PolicyEntryDialog = m_Thinlet.addComponent(m_Thinlet, "/net/coalevo/manager/resources/policy_entry_dialog.xml", this);
    m_Thinlet.putProperty(m_PolicyEntryDialog,"action",a);
    m_Thinlet.setString(m_PolicyEntryDialog, "text", a.getIdentifier());
    //prepare inserts
    prepareRoles();
    preparePermissions();
    //prepare rule
    final AuthorizationRule ar = m_EditPolicy.getRuleFor(a);
    Object editor = m_Thinlet.find(m_PolicyEntryDialog, "authorization.rule");
    m_Thinlet.setString(editor, "text", ar.toSARL());
    m_Thinlet.updateSession();
  }//actionEditPolicy

  public void actionEditPolicyEntryDone() {
    log.debug("actionEditPolicyEntryDone()");
    m_Thinlet.remove(m_PolicyEntryDialog);
    m_PolicyEntryDialog = null;
    m_Thinlet.updateSession();
  }//actionEditPolicyDone

  public void actionAddRole() {
    final Object list = m_Thinlet.find(m_PolicyEntryDialog, "roles.list");
    if (list == null) {
      log.error("Could not find roles list.");
      return;
    }
    final Object item = m_Thinlet.getSelectedItem(list);
    if(item == null) {
      return;
    }
    final Role r = (Role) m_Thinlet.getProperty(item,"role");
    String str = "%"+r.getIdentifier();
    Object editor = m_Thinlet.find(m_PolicyEntryDialog, "authorization.rule");
    str = m_Thinlet.getString(editor,"text") + str;
    m_Thinlet.setString(editor, "text", str);
    m_Thinlet.setInteger(editor, "start", str.length());
    m_Thinlet.setInteger(editor, "end", str.length());
    m_Thinlet.updateSession();
  }//actionAddRole

   public void actionAddPermission() {
    final Object list = m_Thinlet.find(m_PolicyEntryDialog, "permissions.list");
    if (list == null) {
      log.error("Could not find permissions list.");
      return;
    }
    final Object item = m_Thinlet.getSelectedItem(list);
    if(item == null) {
      return;
    }
    final Permission p = (Permission) m_Thinlet.getProperty(item,"permission");
    String str = "%"+p.getIdentifier();
    Object editor = m_Thinlet.find(m_PolicyEntryDialog, "authorization.rule");
    str = m_Thinlet.getString(editor,"text") + str;
    m_Thinlet.setString(editor, "text", str);
    m_Thinlet.setInteger(editor, "start", str.length());
    m_Thinlet.setInteger(editor, "end", str.length());
     m_Thinlet.updateSession();
  }//actionAddPermission

  public void actionCheckRule() {
    final PolicyService ps = m_Services.getPolicyService();
    final Object editor = m_Thinlet.find(m_PolicyEntryDialog, "authorization.rule");
    final String rule = m_Thinlet.getString(editor,"text");
    try {
      ps.createAuthorizationRule(rule);
      m_Thinlet.showInfo(m_Resources.getString("policy.entry.checksuccess"));
    } catch(SARLException ex) {
      m_Thinlet.showError(m_Resources.getString("policy.entry.checkfailed") + " [" + ex.getCause().getMessage() + "]." );
    }
    m_Thinlet.updateSession();
  }//actionCheckRule

  public void actionRevertRule() {
    final Action a = (Action) m_Thinlet.getProperty(m_PolicyEntryDialog, "action");
    final AuthorizationRule ar = m_EditPolicy.getRuleFor(a);
    Object editor = m_Thinlet.find(m_PolicyEntryDialog, "authorization.rule");
    m_Thinlet.setString(editor, "text", ar.toSARL());
    m_Thinlet.updateSession();
  }//actionRevertRule

  public void actionSaveEntry() {
    final PolicyService ps = m_Services.getPolicyService();
    final Action a = (Action) m_Thinlet.getProperty(m_PolicyEntryDialog, "action");
    final Object editor = m_Thinlet.find(m_PolicyEntryDialog, "authorization.rule");
    final String rule = m_Thinlet.getString(editor,"text");
    try {
      final AuthorizationRule ar = ps.createAuthorizationRule(rule);
      if (!ps.putPolicyEntry(m_Agent, m_EditPolicy, a, ar)) {
        m_Thinlet.showError(m_Resources.getString("policy.entry.savefailed") + " [" + a.getIdentifier() + "].");
      } else {
        m_Thinlet.showInfo(m_Resources.getString("policy.entry.savesuccess") + " [" + a.getIdentifier() + "].");
      }

    } catch(SARLException ex) {
      m_Thinlet.showError(m_Resources.getString("policy.entry.checkfailed") + " [" + ex.getCause().getMessage() + "]." );
    }
    m_Thinlet.updateSession();
  }//actionSaveEntry


  private void preparePolicies() {
    try {
      Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "policies.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);

      Set<String> policies = new TreeSet<String>(m_Services.getPolicyService().listAvailablePolicies());
      if (policies != null && policies.size() > 0) {
        for (Iterator<String> iterator = policies.iterator(); iterator.hasNext();) {
          String id = iterator.next();
          addItem(list, id);
        }
      }
    } catch (Exception ex) {
      log.error("preparePolicies()", ex);
    }
  }//preparePolicies

  private void addItem(Object list, String id) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", id);
  }//addItem

  private void prepareEntries() {
    try {
      Object list = m_Thinlet.find(m_PolicyDialog, "entries.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);
      final Set<Action> actions = new TreeSet<Action>(ACTION_COMP);
      actions.addAll(m_EditPolicy.getActions());
      if (actions != null && actions.size() > 0) {
        for (Iterator<Action> iterator = actions.iterator(); iterator.hasNext();) {
          Action a = iterator.next();
          addItem(list, a);
        }
      }
    } catch (Exception ex) {
      log.error("prepareEntries()", ex);
    }
  }//prepareEntries

  private Object addItem(Object list, Action a) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", a.getIdentifier());
    m_Thinlet.putProperty(item, "action", a);
    return item;
  }//addItem

  private void prepareRoles() {
    try {
      Object list = m_Thinlet.find(m_PolicyEntryDialog, "roles.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);

      Set<Role> roles = new TreeSet<Role>(SecurityHandler.ROLE_COMP);
      roles.addAll(m_Services.getSecurityManagementService().getRoles(m_Agent));
      if (roles != null && roles.size() > 0) {
        for (Iterator<Role> iterator = roles.iterator(); iterator.hasNext();) {
          Role r = iterator.next();
          addItem(list, r);
        }
      }
    } catch (Exception ex) {
      log.error("prepareRoles()", ex);
    }
  }//prepareRoles

  private void addItem(Object list, Role r) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", r.getIdentifier());
    m_Thinlet.putProperty(item, "role", r);
  }//addItem


  private void preparePermissions() {
    try {
      Object list = m_Thinlet.find(m_PolicyEntryDialog, "permissions.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);

      Set<Permission> perms = new TreeSet<Permission>(SecurityHandler.PERM_COMP);
        perms.addAll(m_Services.getSecurityManagementService().getPermissions(m_Agent));
      if (perms != null && perms.size() > 0) {
        for (Iterator iterator = perms.iterator(); iterator.hasNext();) {
          Permission p = (Permission) iterator.next();
          addItem(list, p);
        }
      }
    } catch (Exception ex) {
      log.error("preparePermissions()", ex);
    }
  }//preparePermissions

  private void addItem(Object list, Permission p) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", p.getIdentifier());
    m_Thinlet.putProperty(item, "permission", p);
  }//addItem

  private static final Comparator<Action> ACTION_COMP = new Comparator<Action>() {
    public int compare(Action a1,Action a2) {
       return a1.getIdentifier().compareTo(a2.getIdentifier());
    }//compare
  };

}//class PolicyHandler
