/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.manager.impl;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.security.model.Deny;
import net.coalevo.security.model.Permission;
import net.coalevo.security.model.Permit;
import net.coalevo.security.model.Role;
import net.coalevo.security.service.SecurityManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thinlet.Thinlet;

import java.util.*;

/**
 * Provides a {@link Handler} implementation that will handle the UI
 * of the main security panel.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SecurityHandler
    implements Handler {

  private static final Logger log = LoggerFactory.getLogger(SecurityHandler.class);
  private ResourceBundle m_Resources = ResourceBundle.getBundle("net.coalevo.manager.resources.string");

  private ManagerDesktopImpl m_Thinlet;
  private UserAgent m_Agent;
  private ServiceMediator m_Services;
  private Object m_AgentRolesDialog;
  private Object m_RolePermissionsDialog;

  public SecurityHandler(Thinlet thinlet) {
    m_Thinlet = (ManagerDesktopImpl) thinlet;
  }//constructor

  public void init() {
    m_Services = m_Thinlet.getServices();
    m_Agent = m_Thinlet.getUserAgent();
    actionSecurityTabs();
  }//init

  public void deinit() {
    m_Agent = null;
    m_Services = null;
    m_Thinlet = null;
    m_AgentRolesDialog = null;
    m_RolePermissionsDialog = null;
    m_Resources = null;
  }//deinit

  public void actionSecurityTabs() {

    int i = m_Thinlet.getSelectedIndex(m_Thinlet.find(m_Thinlet.getActualPanel(), "security.tabs"));
    if (i <= 0) {
      prepareAgents();
    } else if (i == 1) {
      prepareRoles();
    } else if (i == 2) {
      preparePermissions();
    }
    m_Thinlet.updateSession();
  }//actionSecurityTabs


  public void actionCreateRole() {
    final Object identifier = m_Thinlet.find(m_Thinlet.getActualPanel(), "role.identifier");
    String id = m_Thinlet.getString(identifier, "text");
    if (id == null || id.length() < 3) {
      m_Thinlet.showError(m_Resources.getString("security.roles.nameinvalid") + " [" + id + "].");
      return;
    } else {
      Role r = m_Services.getSecurityManagementService().createRole(m_Agent, id);
      if (r == null) {
        m_Thinlet.showError(m_Resources.getString("security.roles.createfailed") + " [" + id + "].");
      } else {
        addItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "roles.list"), r);
        m_Thinlet.setString(identifier, "text", "");
      }
    }
    m_Thinlet.updateSession();
  }//actionCreateRole

  public void actionDestroyRole() {
    Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "roles.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      Role r = (Role) m_Thinlet.getProperty(item, "role");
      if (m_Services.getSecurityManagementService().destroyRole(m_Agent, r)) {
        m_Thinlet.remove(item);
      } else {
        m_Thinlet.showError(m_Resources.getString("security.roles.removefailed") + " [" + r.getIdentifier() + "].");
      }
    }
    m_Thinlet.updateSession();
  }//actionDestroyRole

  public void actionRegisterAgent() {
    SecurityManagementService sm = m_Services.getSecurityManagementService();
    String id = m_Thinlet.getString(
        m_Thinlet.find(m_Thinlet.getActualPanel(), "agent.identifier"),
        "text"
    );
    boolean user = m_Thinlet.getBoolean(m_Thinlet.find(m_Thinlet.getActualPanel(), "agent.isuser"), "selected");

    if (id == null || id.length() < 3) {
      m_Thinlet.showError(m_Resources.getString("security.agents.nameinvalid") + " [" + id + "].");
      return;
    } else {
      AgentIdentifier aid = new AgentIdentifier(id);
      Agent a = sm.registerAgent(m_Agent, aid, user);
      if (a == null) {
        m_Thinlet.showError(m_Resources.getString("security.agents.registerfailed") + " [" + id + "].");
        return;
      } else {
        addItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "agents.list"), a);
        if (user) {
          String password = m_Services.getRndStringGeneratorService().getRandomString();
          if (!sm.createAuthentication(m_Agent, a, password)) {
            password = m_Resources.getString("failed");
          }
          m_Thinlet.showInfo(m_Resources.getString("security.agents.userregistered") + " [" + id + "::->" + password + "<-].");
        } else {
          m_Thinlet.showInfo(m_Resources.getString("security.agents.serviceregistered") + " [" + id + "].");
        }
        m_Thinlet.setString(m_Thinlet.find(m_Thinlet.getActualPanel(), "agent.identifier"), "text", "");
      }
    }
    m_Thinlet.updateSession();
  }//actionRegisterAgent

  public void actionUnregisterAgent() {
    Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "agents.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      Agent a = (Agent) m_Thinlet.getProperty(item, "agent");
      if (m_Services.getSecurityManagementService().unregisterAgent(m_Agent, a.getAgentIdentifier())) {
        m_Thinlet.remove(item);
      } else {
        m_Thinlet.showError(m_Resources.getString("security.agents.unregisterfailed") + " [" + a.getIdentifier() + "].");
      }
    }
    m_Thinlet.updateSession();
  }//actionUnregisterAgent

  public void actionResetPassword() {
    Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "agents.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      final String passwd = m_Services.getRndStringGeneratorService().getRandomString();
      final Agent a = (Agent) m_Thinlet.getProperty(item, "agent");
      if (m_Services.getSecurityManagementService().updateAuthentication(m_Agent, a, passwd)) {
        m_Thinlet.showInfo(m_Resources.getString("security.agents.passwordresetted") + " [" + a.getIdentifier() + "::" + passwd + "].");
      } else {
        if (m_Services.getSecurityManagementService().createAuthentication(m_Agent, a, passwd)) {
          m_Thinlet.showInfo(m_Resources.getString("security.agents.passwordresetted") + " [" + a.getIdentifier() + "::" + passwd + "].");
        } else {
          m_Thinlet.showError(m_Resources.getString("security.agents.passwordresetfailed"));
        }
      }
    }
    m_Thinlet.updateSession();
  }//resetPassword

  public void actionAgentRoles() {
    Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "agents.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      final Agent a = (Agent) m_Thinlet.getProperty(item, "agent");
      m_AgentRolesDialog = m_Thinlet.addComponent(m_Thinlet, "/net/coalevo/manager/resources/agent_roles_dialog.xml", this);
      m_Thinlet.putProperty(m_AgentRolesDialog, "agent", a);
      m_Thinlet.setString(m_AgentRolesDialog, "text", a.getIdentifier());
      SecurityManagementService sm = m_Services.getSecurityManagementService();
      Set<Role> hasnot = new TreeSet<Role>(ROLE_COMP);
      hasnot.addAll(sm.getRoles(m_Agent));
      Set<Role> has = new TreeSet<Role>(ROLE_COMP);
      has.addAll(sm.getAgentRoles(m_Agent, a));
      Object hasnotlist = m_Thinlet.find(m_AgentRolesDialog, "roles.hasnot.list");
      Object haslist = m_Thinlet.find(m_AgentRolesDialog, "roles.has.list");
      hasnot.removeAll(has);
      if (has != null && has.size() > 0) {
        for (Iterator<Role> iterator = has.iterator(); iterator.hasNext();) {
          Role r = iterator.next();
          addItem(haslist, r);
        }
      }
      if (hasnot != null && hasnot.size() > 0) {
        for (Iterator<Role> iterator = hasnot.iterator(); iterator.hasNext();) {
          Role r = iterator.next();
          addItem(hasnotlist, r);
        }
      }

    }
    m_Thinlet.updateSession();
  }//actionAgentRoles

  public void actionAgentRolesDone() {
    log.debug("actionAgentRolesDone()");
    m_Thinlet.remove(m_AgentRolesDialog);
    m_AgentRolesDialog = null;
    m_Thinlet.updateSession();
  }//actionConfigDone

  public void actionGrantRole() {
    //1. get selected
    final Object hasnotlist = m_Thinlet.find(m_AgentRolesDialog, "roles.hasnot.list");
    final Object haslist = m_Thinlet.find(m_AgentRolesDialog, "roles.has.list");
    final Object item = m_Thinlet.getSelectedItem(hasnotlist);
    if (item == null) {
      return;
    }
    //2. prepare refs
    final SecurityManagementService sm = m_Services.getSecurityManagementService();
    final Agent a = (Agent) m_Thinlet.getProperty(m_AgentRolesDialog, "agent");
    final Role r = (Role) m_Thinlet.getProperty(item, "role");
    if (sm.grantRole(m_Agent, a, r)) {
      m_Thinlet.remove(item);
      addItem(haslist, r);
    } else {
      m_Thinlet.showError(m_Resources.getString("security.roles.grantfailed") + " [" + r.getIdentifier() + "].");
    }
    m_Thinlet.updateSession();
  }//actionGrantRole

  public void actionRevokeRole() {
    //1. get selected
    final Object hasnotlist = m_Thinlet.find(m_AgentRolesDialog, "roles.hasnot.list");
    final Object haslist = m_Thinlet.find(m_AgentRolesDialog, "roles.has.list");
    final Object item = m_Thinlet.getSelectedItem(haslist);
    if (item == null) {
      return;
    }
    //2. prepare refs
    final SecurityManagementService sm = m_Services.getSecurityManagementService();
    final Agent a = (Agent) m_Thinlet.getProperty(m_AgentRolesDialog, "agent");
    final Role r = (Role) m_Thinlet.getProperty(item, "role");
    if (sm.revokeRole(m_Agent, a, r)) {
      m_Thinlet.remove(item);
      addItem(hasnotlist, r);
    } else {
      m_Thinlet.showError(m_Resources.getString("security.roles.revokefailed") + " [" + r.getIdentifier() + "].");
    }
    m_Thinlet.updateSession();
  }//actionRevokeRole

  public void actionCreatePermission() {
    final Object identifier = m_Thinlet.find(m_Thinlet.getActualPanel(), "permission.identifier");
    String id = m_Thinlet.getString(identifier, "text");
    if (id == null || id.length() < 3) {
      m_Thinlet.showError(m_Resources.getString("security.permissions.nameinvalid") + " [" + id + "].");
      return;
    } else {
      Permission p = m_Services.getSecurityManagementService().createPermission(m_Agent, id);
      //System.err.println(p.toString());
      if (p == null) {
        m_Thinlet.showError(m_Resources.getString("security.permissions.createfailed") + " [" + id + "].");
      } else {
        addItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "permissions.list"), p);
        m_Thinlet.setString(identifier, "text", "");
      }
    }
    m_Thinlet.updateSession();
  }//actionCreatePermission

  public void actionDestroyPermission() {
    Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "permissions.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      Permission p = (Permission) m_Thinlet.getProperty(item, "permission");
      if (m_Services.getSecurityManagementService().destroyPermission(m_Agent, p)) {
        m_Thinlet.remove(item);
      } else {
        m_Thinlet.showError(m_Resources.getString("security.permissions.removefailed") + " [" + p.getIdentifier() + "].");
      }
    }
    m_Thinlet.updateSession();
  }//actionDestroyPermission


  public void actionRolePermissions() {
    Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "roles.list");
    Object item = m_Thinlet.getSelectedItem(list);
    if (item == null) {
      return;
    } else {
      final Role r = (Role) m_Thinlet.getProperty(item, "role");
      m_RolePermissionsDialog = m_Thinlet.addComponent(m_Thinlet, "/net/coalevo/manager/resources/role_permissions_dialog.xml", this);
      m_Thinlet.putProperty(m_RolePermissionsDialog, "role", r);
      m_Thinlet.setString(m_RolePermissionsDialog, "text", r.getIdentifier());
      SecurityManagementService sm = m_Services.getSecurityManagementService();
      Set<Permission> hasnot = new TreeSet<Permission>(PERM_COMP);
      hasnot.addAll(sm.getPermissions(m_Agent));
      Set<Permission> has = new TreeSet<Permission>(PERM_COMP);
      has.addAll(sm.getRolePermissions(m_Agent, r));
      //System.err.println("Has: " +has);
      //System.err.println("HasNot: " + hasnot);

      Object hasnotlist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.hasnot.list");
      Object haslist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.has.list");
      hasnot.removeAll(has);
      if (has != null && has.size() > 0) {
        for (Iterator<Permission> iterator = has.iterator(); iterator.hasNext();) {
          Permission p = iterator.next();
          addItem(haslist, p);
        }
      }
      if (hasnot != null && hasnot.size() > 0) {
        for (Iterator<Permission> iterator = hasnot.iterator(); iterator.hasNext();) {
          Permission p = iterator.next();
          addItem(hasnotlist, p);
        }
      }

    }
    m_Thinlet.updateSession();
  }//actionRolePermissions


  public void actionRolePermissionsDone() {
    log.debug("actionRolePermissionsDone()");
    m_Thinlet.remove(m_RolePermissionsDialog);
    m_RolePermissionsDialog = null;
    m_Thinlet.updateSession();
  }//actionRolePermissionsDone


  public void actionAddPermit() {
    //1. get selected
    final Object hasnotlist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.hasnot.list");
    final Object haslist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.has.list");
    final Object item = m_Thinlet.getSelectedItem(hasnotlist);
    if (item == null) {
      return;
    }
    //2. prepare refs
    final SecurityManagementService sm = m_Services.getSecurityManagementService();
    final Role r = (Role) m_Thinlet.getProperty(m_RolePermissionsDialog, "role");
    Permission p = (Permission) m_Thinlet.getProperty(item, "permission");
    p = sm.getPermit(p);
    if (sm.addRolePermission(m_Agent, r, p)) {
      m_Thinlet.remove(item);
      addItem(haslist, p);
    } else {
      m_Thinlet.showError(m_Resources.getString("security.permissions.addpermitfailed") + " [" + p.getIdentifier() + "].");
    }
    m_Thinlet.updateSession();
  }//actionAddPermit

  public void actionAddDeny() {
    //1. get selected
    final Object hasnotlist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.hasnot.list");
    final Object haslist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.has.list");
    final Object item = m_Thinlet.getSelectedItem(hasnotlist);
    if (item == null) {
      return;
    }
    //2. prepare refs
    final SecurityManagementService sm = m_Services.getSecurityManagementService();
    final Role r = (Role) m_Thinlet.getProperty(m_RolePermissionsDialog, "role");
    Permission p = (Permission) m_Thinlet.getProperty(item, "permission");
    p = sm.getDeny(p);
    if (sm.addRolePermission(m_Agent, r, p)) {
      m_Thinlet.remove(item);
      addItem(haslist, p);
    } else {
      m_Thinlet.showError(m_Resources.getString("security.permissions.adddenyfailed") + " [" + p.getIdentifier() + "].");
    }
    m_Thinlet.updateSession();
  }//actionAddDeny

  public void actionRemovePermission() {
    //1. get selected
    final Object hasnotlist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.hasnot.list");
    final Object haslist = m_Thinlet.find(m_RolePermissionsDialog, "permissions.has.list");
    final Object item = m_Thinlet.getSelectedItem(haslist);
    if (item == null) {
      return;
    }
    //2. prepare refs
    final SecurityManagementService sm = m_Services.getSecurityManagementService();
    final Role r = (Role) m_Thinlet.getProperty(m_RolePermissionsDialog, "role");
    final Permission p = (Permission) m_Thinlet.getProperty(item, "permission");
    if (sm.removeRolePermission(m_Agent, r, p)) {
      m_Thinlet.remove(item);
      addItem(hasnotlist, sm.getPermission(p));
    } else {
      m_Thinlet.showError(m_Resources.getString("security.permissions.removepermissionfailed") + " [" + p.getIdentifier() + "].");
    }
    m_Thinlet.updateSession();
  }//actionRemovePermission


  private void prepareRoles() {
    try {
      Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "roles.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);

      Set<Role> roles = new TreeSet<Role>(ROLE_COMP);
      roles.addAll(m_Services.getSecurityManagementService().getRoles(m_Agent));
      if (roles != null && roles.size() > 0) {
        for (Iterator<Role> iterator = roles.iterator(); iterator.hasNext();) {
          Role r = iterator.next();
          addItem(list, r);
        }
      }
    } catch (Exception ex) {
      log.error("prepareRoles()", ex);
    }
  }//prepareRoles

  private void addItem(Object list, Role r) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", r.getIdentifier());
    m_Thinlet.putProperty(item, "role", r);
  }//addItem

  private void prepareAgents() {
    try {
      Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "agents.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);

      Set<Agent> agents = new TreeSet<Agent>(AGENT_COMP);
      agents.addAll(m_Services.getSecurityManagementService().getAgents(m_Agent));
      if (agents != null && agents.size() > 0) {
        for (Iterator<Agent> iterator = agents.iterator(); iterator.hasNext();) {
          Agent a = iterator.next();
          addItem(list, a);
        }
      }
    } catch (Exception ex) {
      log.error("prepareAgents()", ex);
    }
  }//prepareAgents

  private void addItem(Object list, Agent a) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", a.getAgentIdentifier().getName());
    m_Thinlet.putProperty(item, "agent", a);
    if (a instanceof UserAgent) {
      m_Thinlet.setIcon(item, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/agent.png"));
    } else {
      m_Thinlet.setIcon(item, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/bundle-stopped.png"));
    }
  }//addItem

  private void preparePermissions() {
    try {
      Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "permissions.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);

      Set<Permission> perms = new TreeSet<Permission>(PERM_COMP);
      perms.addAll(m_Services.getSecurityManagementService().getPermissions(m_Agent));
      if (perms != null && perms.size() > 0) {
        for (Iterator iterator = perms.iterator(); iterator.hasNext();) {
          Permission p = (Permission) iterator.next();
          addItem(list, p);
        }
      }
    } catch (Exception ex) {
      log.error("preparePermissions()", ex);
    }

  }//preparePermissions

  private void addItem(Object list, Permission p) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", p.getIdentifier());
    m_Thinlet.putProperty(item, "permission", p);
    //System.err.println(p.toString());
    if (p instanceof Permit) {
      m_Thinlet.setIcon(item, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/permit.png"));
    } else if (p instanceof Deny) {
      m_Thinlet.setIcon(item, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/deny.png"));
    }

  }//addItem


  private static final Comparator<Agent> AGENT_COMP = new Comparator<Agent>() {
     public int compare(Agent a1, Agent a2) {
       if(a1 instanceof UserAgent && a2 instanceof ServiceAgent) {
         return -1;
       }
       if(a1 instanceof ServiceAgent && a2 instanceof UserAgent) {
         return 1;
       }
       return a1.getIdentifier().compareTo(a2.getIdentifier());
     }//compare
  };

  public static final Comparator<Role> ROLE_COMP = new Comparator<Role>() {
    public int compare(Role r1, Role r2) {
      return r1.getIdentifier().compareTo(r2.getIdentifier());
    }//compare
  };

  public static final Comparator<Permission> PERM_COMP = new Comparator<Permission>() {
    public int compare(Permission p1,Permission p2) {
       if(p1 instanceof Permit && p2 instanceof Deny) {
         return -1;
       }
       if(p1 instanceof Deny && p2 instanceof Permit) {
         return 1;
       }
       return p1.getIdentifier().compareTo(p2.getIdentifier());

    }//compare
  };

}//class SecurityHandler
