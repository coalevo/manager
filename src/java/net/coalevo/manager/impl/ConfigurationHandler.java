/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.manager.impl;

import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.foundation.util.metatype.NoSuchAttributeException;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.metatype.AttributeDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thinlet.Thinlet;

import java.io.IOException;
import java.util.*;

/**
 * Provides a {@link Handler} implementation that will
 * handle the UI of the configuration panel.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ConfigurationHandler implements Handler {

  private static final Logger log = LoggerFactory.getLogger(ConfigurationHandler.class);
  private ResourceBundle m_Resources = ResourceBundle.getBundle("net.coalevo.manager.resources.string");

  private ManagerDesktopImpl m_Thinlet;
  private ConfigurationAdmin m_ConfigAdmin;
  private Object m_ConfigurationDialog;
  private MetaTypeDictionary m_EditDictionary;
  private Configuration m_EditConfiguration;

  public ConfigurationHandler(Thinlet thinlet) {
    m_Thinlet = (ManagerDesktopImpl) thinlet;
    m_ConfigAdmin = m_Thinlet.getServices().getConfigurationAdmin();
  }//constructor

  public void init() {
    actionConfigTabs();
  }//init

  public void actionConfigTabs() {
    int i = m_Thinlet.getSelectedIndex(m_Thinlet.find(m_Thinlet.getActualPanel(), "configuration.tabs"));
    if (i <= 0) {
      prepareConfigurations();
    } else if (i == 1) {
      prepareFactories();
    }
  }//actionConfigTabs

  private void prepareConfigurations() {
    try {
      Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "configurations.list");
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      //rebuild list
      m_Thinlet.removeAll(list);
      Configuration[] confs = m_ConfigAdmin.listConfigurations(null);
      Arrays.sort(confs,CONFIG_COMP);
      if (confs != null && confs.length > 0) {
        for (int i = 0; i < confs.length; i++) {
          Configuration conf = confs[i];
          addItem(list, conf);
        }
      }
    } catch (Exception ex) {
      log.error("prepareConfigurations()", ex);
    }

  }//prepareConfigurations

  private void prepareFactories() {
    //2. Get all factories
    ServiceReference[] srefs = null;
    try {
      srefs = Activator.getBundleContext().getServiceReferences("org.osgi.service.cm.ManagedServiceFactory", null);
    } catch (InvalidSyntaxException ex) {
    }
    if (srefs != null && srefs.length > 0) {
      Object list = m_Thinlet.find(m_Thinlet.getActualPanel(), "factories.list");
      m_Thinlet.removeAll(list);
      if (list == null) {
        log.error("Could not find list.");
        return;
      }
      Arrays.sort(srefs,SERVICEREF_COMP);
      for (int i = 0; i < srefs.length; i++) {
        addItem(list, srefs[i]);
      }
    }
  }//prepareFactories

  public void actionCreateFactoryInstance() {
    log.debug("actionCcreateFactoryInstance()");
    //1. get selected
    Object item = m_Thinlet.getSelectedItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "factories.list"));
    if (item == null) {
      return;
    }
    String pid = m_Thinlet.getString(item, "text");
    try {
      m_EditConfiguration = m_ConfigAdmin.createFactoryConfiguration(pid, Activator.findBundle(pid).getLocation());
    } catch (IOException e) {
      log.error("actionCreateConfiguration()", e);
      m_Thinlet.showError(e.getMessage());
      return;
    }
    createConfigDialog(pid, true);
  }//actionCreateFactoryInstance

  public void actionEditConfiguration() {
    log.debug("actionEditConfiguration()");
    //1. get selected
    Object item = m_Thinlet.getSelectedItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "configurations.list"));
    if (item == null) {
      return;
    }
    String pid = m_Thinlet.getString(item, "text");
    try {
      m_EditConfiguration = m_ConfigAdmin.getConfiguration(pid);
      final String fpid = m_EditConfiguration.getFactoryPid();
      if (fpid != null) {
        pid = fpid;
      }
    } catch (IOException e) {
      log.error("actionEditConfiguration()", e);
      m_Thinlet.showError(e.getMessage());
      return;
    }
    createConfigDialog(pid, false);
  }//actionEditConfig

  public void actionDeleteConfiguration() {
    log.debug("actionDeleteConfiguration()");
    //1. get selected
    final Object item = m_Thinlet.getSelectedItem(m_Thinlet.find(m_Thinlet.getActualPanel(), "configurations.list"));
    if (item == null) {
      return;
    }
    String pid = m_Thinlet.getString(item, "text");
    //need to get config from admin
    try {
      final Configuration cfg = m_ConfigAdmin.getConfiguration(pid);
      cfg.delete();
      m_Thinlet.remove(item);
    } catch (Exception ex) {
      log.error("actionDeleteConfiguration",ex);
      m_Thinlet.showError(ex.getMessage());
    }
  }//actionDeleteConfiguration

  private void createConfigDialog(String pid, boolean factory) {
    m_ConfigurationDialog = m_Thinlet.addComponent(m_Thinlet, "/net/coalevo/manager/resources/config_dialog.xml", this);
    m_Thinlet.setString(m_ConfigurationDialog, "text", pid);
    Object panel = m_Thinlet.find(m_ConfigurationDialog, "configuration.dynaform");
    try {
      if (factory) {
        m_Thinlet.setIcon(m_ConfigurationDialog, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/factoryservice-config.png"));

        m_EditDictionary = new MetaTypeDictionary(null, Activator.getBundleContext(), Activator.findBundle(pid), pid);
        m_EditDictionary.getDictionary();
      } else {
        m_Thinlet.setIcon(m_ConfigurationDialog, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/service-config.png"));
        m_EditDictionary = new MetaTypeDictionary(m_EditConfiguration.getProperties(), Activator.getBundleContext(), Activator.findBundle(pid), pid);
      }
      Set s = new TreeSet(m_EditDictionary.keySet());
      for (Iterator iter = s.iterator(); iter.hasNext();) {
        String key = (String) iter.next();
        final AttributeDefinition ad = m_EditDictionary.getDefinition(key);

        if (m_EditDictionary.hasOptions(key)) {
          createOptionEditor(panel, ad, m_EditDictionary.getOptions(key), m_EditDictionary.getValue(key));

        } else {
          switch (ad.getType()) {
            case AttributeDefinition.BOOLEAN:
              createBooleanEditor(panel, ad, m_EditDictionary.getBoolean(key).booleanValue());
              break;
            case AttributeDefinition.BYTE:
            case AttributeDefinition.INTEGER:
            case AttributeDefinition.SHORT:
              createIntegerEditor(panel, ad, m_EditDictionary.getValue(key));
              break;
            case AttributeDefinition.DOUBLE:
            case AttributeDefinition.FLOAT:
            case AttributeDefinition.LONG:
              createStringEditor(panel, ad, m_EditDictionary.getValue(key));
              break;
            case AttributeDefinition.STRING:
            case AttributeDefinition.CHARACTER:
              createStringEditor(panel, ad, m_EditDictionary.getString(key));
              break;
            default:
              log.error("Not implemented yet.");
          }
        }
        if (iter.hasNext()) {
          Object sep = Thinlet.create("separator");
          m_Thinlet.add(panel, sep);
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      m_Thinlet.showError(m_Resources.getString("config.error.ocd") + " [" + pid + "]");
      actionConfigDone();
      return;
    }
  }//createDynaForm

  public void actionConfigDone() {
    log.debug("actionConfigDone()");
    m_Thinlet.remove(m_ConfigurationDialog);
    m_ConfigurationDialog = null;
  }//actionConfigDone

  public void actionConfigSave() {
    log.debug("actionConfigSave()");
    try {
      m_EditConfiguration.update(m_EditDictionary.getDictionary());
      m_Thinlet.showInfo(m_Resources.getString("config.update.success"));
    } catch (Exception ex) {
      m_Thinlet.showError(ex.getMessage());
    }
  }//actionConfigSave

  public void deinit() {
    //To change body of implemented methods use File | Settings | File Templates.
  }//deinit

  public void addItem(Object list, Configuration cfg) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", cfg.getPid());
    if (cfg.getFactoryPid() == null) {
      m_Thinlet.setIcon(item, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/factoryservice-config.png"));
    } else {
      m_Thinlet.setIcon(item, "icon", m_Thinlet.getIcon("net/coalevo/manager/resources/images/service-config.png"));
    }
  }//addItem


  public void addItem(Object list, ServiceReference ref) {
    Object item = Thinlet.create("item");
    m_Thinlet.add(list, item);
    m_Thinlet.setString(item, "text", ref.getProperty(Constants.SERVICE_PID).toString());
  }//createItem

  public void setBoolean(String text) {
    if (m_EditDictionary != null) {
      final int pidx = text.lastIndexOf('.');
      String key = text.substring(0, pidx);
      try {
        m_EditDictionary.setValue(key, text.substring(pidx + 1, text.length()));
      } catch (NoSuchAttributeException ex) {
        log.error("setBoolean()", ex);
      }
      //System.err.println(key + "::" + text.substring(pidx + 1, text.length()));
    }

//    System.err.println(m_Thinlet.getString(m_ConfigurationDialog, "name"));
  }//setBoolean

  public void setValue(String key, String val) {
    if (m_EditDictionary != null) {
      try {
        m_EditDictionary.setValue(key, val);
      } catch (MetaTypeDictionaryException ex) {
        log.error("setText()", ex);
      }
      //System.err.println(key + "::" + val);
    }
  }//setValue

  public void setOption(Object cbox) {
    Object choice = m_Thinlet.getSelectedItem(cbox);
    final String key = m_Thinlet.getString(cbox, "name");
    final String val = m_Thinlet.getString(choice, "name");
    try {
      m_EditDictionary.setValue(key, val);
    } catch (MetaTypeDictionaryException ex) {
      log.error("setOption()", ex);
    }
    //System.err.println(key + "::" + val);
  }//setOption


  private void createBooleanEditor(Object parent, AttributeDefinition ad, boolean selected) {

    Object panel = Thinlet.create("panel");
    m_Thinlet.setInteger(panel, "weightx", 1);
    m_Thinlet.setInteger(panel, "columns", 3);
    m_Thinlet.setInteger(panel, "gap", 3);
    m_Thinlet.add(parent, panel);

    Object label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getName() + ":");
    m_Thinlet.add(panel, label);

    Object tbox = Thinlet.create("checkbox");
    m_Thinlet.setString(tbox, "name", ad.getID() + ".true");
    m_Thinlet.setString(tbox, "group", ad.getID() + ".group");
    m_Thinlet.setString(tbox, "text", m_Resources.getString("true"));
    m_Thinlet.setMethod(tbox, "action", "setBoolean(this.name)", panel, this);
    m_Thinlet.setBoolean(tbox, "selected", selected);
    m_Thinlet.add(panel, tbox);

    tbox = Thinlet.create("checkbox");
    m_Thinlet.setString(tbox, "name", ad.getID() + ".false");
    m_Thinlet.setString(tbox, "group", ad.getID() + ".group");
    m_Thinlet.setString(tbox, "text", m_Resources.getString("false"));
    m_Thinlet.setMethod(tbox, "action", "setBoolean(this.name)", panel, this);
    m_Thinlet.setBoolean(tbox, "selected", !selected);
    m_Thinlet.add(panel, tbox);

    label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getDescription());
    m_Thinlet.setInteger(label, "colspan", 3);
    m_Thinlet.add(panel, label);

/*
<?xml version="1.0" encoding="utf-8"?>
<panel columns="3" gap="7">
    <label text="property.key:"/>
    <checkbox group="property" selected="true" text="i18n.true"/>
    <checkbox group="property" text="i18n.false"/>
</panel>
*/

  }//createBoolean

  private void createStringEditor(Object parent, AttributeDefinition ad, String text) {
    Object panel = Thinlet.create("panel");
    m_Thinlet.setInteger(panel, "weightx", 1);
    m_Thinlet.setInteger(panel, "columns", 2);
    m_Thinlet.setInteger(panel, "gap", 3);
    m_Thinlet.add(parent, panel);

    Object label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getName() + ":");
    m_Thinlet.add(panel, label);

    Object textfield = Thinlet.create("textfield");
    m_Thinlet.setString(textfield, "name", ad.getID());
    m_Thinlet.setString(textfield, "text", text);
    m_Thinlet.setInteger(textfield, "weightx", 1);
    m_Thinlet.setMethod(textfield, "action", "setValue(this.name,this.text)", panel, this);
    m_Thinlet.add(panel, textfield);

    label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getDescription());
    m_Thinlet.setInteger(label, "colspan", 2);
    m_Thinlet.add(panel, label);

  }//createStringEditor

  private void createOptionEditor(Object parent, AttributeDefinition ad, Map options, Object selected) {
    Object panel = Thinlet.create("panel");
    m_Thinlet.setInteger(panel, "weightx", 1);
    m_Thinlet.setInteger(panel, "columns", 2);
    m_Thinlet.setInteger(panel, "gap", 3);
    m_Thinlet.add(parent, panel);

    Object label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getName() + ":");
    m_Thinlet.add(panel, label);

    Object field = Thinlet.create("combobox");
    m_Thinlet.setString(field, "name", ad.getID());
    m_Thinlet.setBoolean(field, "editable", false);
    m_Thinlet.setMethod(field, "action", "setOption(this)", panel, this);
    m_Thinlet.add(panel, field);

    int i = 0;
    for (Iterator iterator = options.entrySet().iterator(); iterator.hasNext(); i++) {
      Map.Entry entry = (Map.Entry) iterator.next();
      String ol = (String) entry.getKey();
      String ov = (String) entry.getValue();
      Object choice = Thinlet.create("choice");
      m_Thinlet.setString(choice, "name", ov);
      m_Thinlet.setString(choice, "text", ol);
      m_Thinlet.add(field, choice);
      if (selected.equals(ov)) {
        m_Thinlet.setInteger(field, "selected", i);
      }

    }

    label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getDescription());
    m_Thinlet.setInteger(label, "colspan", 2);
    m_Thinlet.add(panel, label);

  }//createOptionEditor

  private void createIntegerEditor(Object parent, AttributeDefinition ad, String val) {
    Object panel = Thinlet.create("panel");
    m_Thinlet.setInteger(panel, "weightx", 1);
    m_Thinlet.setInteger(panel, "columns", 2);
    m_Thinlet.setInteger(panel, "gap", 3);
    m_Thinlet.add(parent, panel);

    Object label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getName() + ":");
    m_Thinlet.add(panel, label);

    Object sbox = Thinlet.create("spinbox");
    m_Thinlet.setString(sbox, "name", ad.getID());
    m_Thinlet.setString(sbox, "text", val);
    int min = Integer.MIN_VALUE;
    int max = Integer.MAX_VALUE;
    switch (ad.getType()) {
      case AttributeDefinition.BYTE:
        min = Byte.MIN_VALUE;
        max = Byte.MAX_VALUE;
        break;
      case AttributeDefinition.SHORT:
        min = Short.MIN_VALUE;
        max = Short.MAX_VALUE;
        break;
    }
    m_Thinlet.setInteger(sbox, "minimum", min);
    m_Thinlet.setInteger(sbox, "maximum", max);
    m_Thinlet.setMethod(sbox, "action", "setValue(this.name,this.text)", panel, this);
    m_Thinlet.add(panel, sbox);

    label = Thinlet.create("label");
    m_Thinlet.setString(label, "text", ad.getDescription());
    m_Thinlet.setInteger(label, "colspan", 2);
    m_Thinlet.add(panel, label);

  }//createIntegerEditor

  private static final Comparator SERVICEREF_COMP = new Comparator() {

     public int compare(Object o1, Object o2) {
       ServiceReference ref1 = (ServiceReference) o1;
       ServiceReference ref2 = (ServiceReference) o2;
       return ref1.getProperty(Constants.SERVICE_PID).toString().compareTo(
           ref2.getProperty(Constants.SERVICE_PID).toString()
       );
     }//compare

  };

  private static final Comparator CONFIG_COMP = new Comparator() {
    public int compare(Object o1, Object o2) {
       Configuration cfg1 = (Configuration) o1;
       Configuration cfg2 = (Configuration) o2;
       return cfg1.getPid().compareTo(
           cfg2.getPid()
       );
     }//compare
  };

}//class ConfigurationHandler
