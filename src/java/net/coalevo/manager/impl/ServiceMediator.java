/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.manager.impl;

import org.osgi.framework.*;
import org.osgi.service.metatype.MetaTypeService;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import net.coalevo.security.service.SecurityService;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.PolicyXMLService;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.SessionService;
import net.coalevo.foundation.service.RndStringGeneratorService;

/**
 * Provides a mediator for required Coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ServiceMediator {

  private static final Logger log = LoggerFactory.getLogger(ServiceMediator.class);
  private BundleContext m_BundleContext;
  private ServiceListener m_ServiceListener;

  private SecurityService m_SecurityService;
  private PolicyService m_PolicyService;
  private ExecutionService m_ExecutionService;
  private SecurityManagementService m_SecurityMgmtService;
  private MetaTypeService m_MetaTypeService;
  private SessionService m_SessionService;
  private RndStringGeneratorService m_RndStringGeneratorService;
  private ConfigurationAdmin m_ConfigurationAdmin;
  private PolicyXMLService m_PolicyXMLService;

  public SecurityService getSecurityService() {
    return m_SecurityService;
  }//getSecurityService

  public PolicyService getPolicyService() {
    return m_PolicyService;
  }//getPolicyService

  public ExecutionService getExecutionService() {
    return m_ExecutionService;
  }//getExecutionService

  public SessionService getSessionService() {
    return m_SessionService;
  }//getSessionService

  public MetaTypeService getMetaTypeService() {
    return m_MetaTypeService;
  }//getMetaTypeService

  public SecurityManagementService getSecurityManagementService() {
    return m_SecurityMgmtService;
  }//getSecurityManagementService

  public RndStringGeneratorService getRndStringGeneratorService() {
    return m_RndStringGeneratorService;
  }//getRandomStringGeneratorService

  public ConfigurationAdmin getConfigurationAdmin() {
    return m_ConfigurationAdmin;
  }//getConfigurationAdmin

  public PolicyXMLService getPolicyXMLService() {
    return m_PolicyXMLService;
  }//getPolicyXMLService

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepareDefinitions listener
    m_ServiceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(|(|(|(objectclass=" + SecurityService.class.getName() + ")" +
            "(objectclass=" + PolicyService.class.getName() + "))" +
            "(objectclass=" + PolicyXMLService.class.getName() + "))" +
            "(objectclass=" + SecurityManagementService.class.getName() + "))" +
            "(objectclass=" + MetaTypeService.class.getName() + "))" +
            "(objectclass=" + RndStringGeneratorService.class.getName() + "))" +
            "(objectclass=" + ConfigurationAdmin.class.getName() + "))" +
            "(objectclass=" + SessionService.class.getName() + "))" ;

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(m_ServiceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        m_ServiceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      log.error("activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {

    //remove the listener
    try {
    m_BundleContext.removeServiceListener(m_ServiceListener);
    } catch (Exception ex) {
      
    }
    //null out the references
    m_ServiceListener = null;
    m_SecurityService = null;
    m_PolicyService = null;
    m_PolicyXMLService = null;
    m_ExecutionService = null;
    m_SecurityMgmtService = null;
    m_SessionService = null;
    m_MetaTypeService = null;
    m_RndStringGeneratorService = null;
    m_BundleContext = null;
  }//deactivate

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:

          o = m_BundleContext.getService(sr);
          if (o == null) {
            log.error("ServiceListener:Registration:NULL");
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = (SecurityService) o;
            log.info("ServiceListener:Registration:SecurityService");
          } else if (o instanceof PolicyService) {
            m_PolicyService = (PolicyService) o;
            log.info("ServiceListener:Registration:PolicyService");
          } else if (o instanceof PolicyXMLService) {
            m_PolicyXMLService = (PolicyXMLService) o;
            log.info("ServiceListener:Registration:PolicyXMLService");
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = (ExecutionService) o;
            log.info("ServiceListener:Registration:ExecutionService");
          } else if (o instanceof SecurityManagementService) {
            m_SecurityMgmtService = (SecurityManagementService) o;
            log.info("ServiceListener:Registration:SecurityManagementService");
          } else if (o instanceof SessionService) {
            m_SessionService = (SessionService) o;
            log.info("ServiceListener:Registration:SessionService");
          } else if (o instanceof MetaTypeService) {
            m_MetaTypeService = (MetaTypeService) o;
            log.info("ServiceListener:Registration:MetaTypeService");
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = (RndStringGeneratorService) o;
            log.info("ServiceListener:Registration:RndStringGeneratorService");
          } else if (o instanceof ConfigurationAdmin) {
            m_ConfigurationAdmin = (ConfigurationAdmin) o;
            log.info("ServiceListener:Registration:ConfigurationAdmin");
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            log.error("ServiceListener:Unregistration::null");
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = null;
            log.info("ServiceListener:Unregistration:SecurityService " + m_SecurityService.toString());
          } else if (o instanceof PolicyService) {
            m_PolicyService = null;
            log.info("ServiceListener:Unregistration:PolicyService"
                + m_PolicyService.toString());
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = null;
            log.info("ServiceListener:Unregistration:ExecutionService");
          } else if (o instanceof SecurityManagementService) {
            m_SecurityMgmtService = null;
            log.info("ServiceListener:Unregistration:SecurityManagementService " + m_SecurityMgmtService.toString());
          }  else if (o instanceof SessionService) {
            m_SessionService = null;
            log.info("ServiceListener:Unregistration:MetaService " + m_SessionService.toString());
          } else if (o instanceof MetaTypeService) {
            m_MetaTypeService = null;
            log.info("ServiceListener:Unregistration:MetaTypeService");
          }  else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = null;
            log.info("ServiceListener:Unregistration:RndStringGeneratorService");
          } else if (o instanceof ConfigurationAdmin) {
            m_ConfigurationAdmin = null;
            log.info("ServiceListener:Registration:ConfigurationAdmin");
          } else if (o instanceof PolicyXMLService) {
            m_PolicyXMLService = null;
            log.info("ServiceListener:Unregistration:PolicyXMLService");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

}//class ServiceMediator
