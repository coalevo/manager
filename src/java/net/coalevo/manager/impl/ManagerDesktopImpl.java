/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.manager.impl;

import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.SessionListener;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.system.service.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thinlet.Thinlet;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/**
 * Implements the Manager Desktop.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ManagerDesktopImpl extends Thinlet implements ManagerDesktop {

  private static final Logger log = LoggerFactory.getLogger(ManagerDesktopImpl.class);
  private ResourceBundle m_Resources = ResourceBundle.getBundle("net.coalevo.manager.resources.string");
  private Object m_Dialog;
  private Object m_ActualPanel;
  private UserAgent m_UserAgent;
  private Session m_Session;
  private boolean m_LoggedIn = false;
  private Object m_ErrorDialog;
  private Object m_InfoDialog;
  private Handler m_LastHandler;
  private ServiceMediator m_Services;
  private SessionListener m_SessionListener;
  private Map m_Icons = new HashMap();

  public ManagerDesktopImpl() {

    try {
      this.setResourceBundle(m_Resources);
      add(parse("/net/coalevo/manager/resources/desktop.xml"));
      m_ErrorDialog = addComponent(null, "/net/coalevo/manager/resources/error_dialog.xml", null, null);
      m_InfoDialog = addComponent(null, "/net/coalevo/manager/resources/info_dialog.xml", null, null);
    } catch (IOException ex) {
      log.error("Could not prepare Desktop.");
    }
  }//constructor

  public boolean isLoggedIn() {
    return m_LoggedIn;
  }//isLoggedIn

  public void actionAbout() {
    showInfo(m_Resources.getString("about"));
  }//actionAbout

  public void actionLogin() {
    //dialog
    log.debug("actionLogin()");
    m_Dialog = addComponent(this, "/net/coalevo/manager/resources/login_dialog.xml", null, null);
  }//actionLogin

  public void doLogin(Object user, Object password) {
    log.debug("doLogin()::" + user.toString());
    try {
      //1. retrieve credentials
      final String uname = getString(user, "text");
      final String passwd = getString(password, "text");
      //3. get session
      SessionService sess = m_Services.getSessionService();
      if (sess == null) {
        showError(m_Resources.getString("session.unavailable"));
        return;
      } else {
        try {
          m_Session = sess.initiateSession(uname, passwd, 15*60, TimeUnit.SECONDS);
          m_UserAgent = m_Session.getAgent();
          m_SessionListener = new SessionListener() {
            public void invalidated(Session s) {
              doLogout();
            }
          };
          m_Session.addSessionListener(m_SessionListener);
          setAuthStatus(true);
        } catch (SecurityException ex) {
          showError(m_Resources.getString("session.failed"));
          return;
        }
      }
    } finally {
      remove(m_Dialog);
    }
  }//doLogin

  public void doLogout() {
    log.debug("doLogout()");
    //1. session
    if (m_Session != null) {
      m_Session.removeSessionListener(m_SessionListener);
      if (m_Session.isValid()) {
        m_Session.invalidate(); //will deauthenticate actually :)
      }
    }
    setAuthStatus(false);
  }//doLogout

  private void setAuthStatus(boolean b) {
    //change session menu action
    setBoolean(find("menuitem.login"), "enabled", !b);
    setBoolean(find("menuitem.logout"), "enabled", b);
    //activate menus
    setBoolean(find("menu.security"), "enabled", b);
    //change status icon
    setIcon(find("auth.status"), "icon", getIcon((b) ? AUTH_STATUS_LOGGEDIN : AUTH_STATUS_LOGGEDOUT));
    updateSessionInfo(b);
    removeActualPanel();
    m_LoggedIn = b;
  }//setLoggedIn

  private void updateSessionInfo(boolean b) {
    if (b) {
      updateSessionInfo();
    } else {
      setString(find("menuitem.sessioninfo"), "text", "");
    }
  }//updateSessionInfo

  public void updateSessionInfo() {
    if (m_Session != null) {
      setString(find("menuitem.sessioninfo"), "text", m_Session.toString());
    }
  }//updateSession

  public void updateSession() {
    if(m_Session!=null) {
      m_Session.access();
    }
  }//updateSession


  public void actionAuthentication() {
    log.debug("actionAuthentication()");
  }//actionAuthentication

  public void actionRoles() {
    log.debug("actionRoles()");

  }//actionRoles

  public void actionBundles() {
    log.debug("actionBundles()");
    removeActualPanel();
    m_ActualPanel = addComponent(
        find("desktop.content"), "/net/coalevo/manager/resources/bundles_panel.xml",
        "net.coalevo.manager.impl.FrameworkHandler", null);
    if (m_LastHandler != null) {
      m_LastHandler.init();
    }
  }//actionBundles

  public void actionConfiguration() {
    log.debug("actionConfiguration()");
    removeActualPanel();
    m_ActualPanel = addComponent(
        find("desktop.content"), "/net/coalevo/manager/resources/configuration_panel.xml",
        "net.coalevo.manager.impl.ConfigurationHandler", null);
    if (m_LastHandler != null) {
      m_LastHandler.init();
    }
  }//actionConfiguration

  public void actionSecurity() {
    log.debug("actionSecurity()");
    removeActualPanel();
    m_ActualPanel = addComponent(
        find("desktop.content"), "/net/coalevo/manager/resources/security_panel.xml",
        "net.coalevo.manager.impl.SecurityHandler", null);
    if (m_LastHandler != null) {
      m_LastHandler.init();
    }
    updateSession();
  }//actionSecurity

  public void actionPolicies() {
    log.debug("actionPolicies()");
    removeActualPanel();
    m_ActualPanel = addComponent(
        find("desktop.content"), "/net/coalevo/manager/resources/policies_panel.xml",
        "net.coalevo.manager.impl.PolicyHandler", null);
    if (m_LastHandler != null) {
      m_LastHandler.init();
    }
    updateSession();
  }//actionPolicies

  private void removeActualPanel() {
    if (m_ActualPanel != null) {
      remove(m_ActualPanel);
      m_ActualPanel = null;
    }
    if (m_LastHandler != null) {
      m_LastHandler.deinit();
      m_LastHandler = null;
    }
  }//removeActualPanel


  public Object getActualPanel() {
    log.debug("getActualBundle()");
    return m_ActualPanel;
  }//getActualPanel

  public UserAgent getUserAgent() {
    return m_UserAgent;
  }//getUserAgent

  public ServiceMediator getServices() {
    return m_Services;
  }//getServices

  /**
   * Show prepared modal error.
   *
   * @param msg error message
   */
  public void showError(String msg) {
    Object fMsg = find(m_ErrorDialog, "msg");
    setString(fMsg, "text", msg);
    add(m_ErrorDialog);
  }//showError

  /**
   * Show prepared modal info.
   *
   * @param msg error message
   */
  public void showInfo(String msg) {
    Object fMsg = find(m_InfoDialog, "msg");
    setString(fMsg, "text", msg);
    add(m_InfoDialog);
  }//showInfo

  /**
   * Add a Thinlet component from XUL file.
   *
   * @param parent     add the new component to this parent
   * @param compView   path to the XUL resource
   * @param handlerStr fully qualified classname of the handler to instantiate,
   *                   or null if the current class will become the handler
   * @param argv       if not null, these arguments will be passed to the
   *                   appropriate constructor.
   */
  public Object addComponent(Object parent, String compView, String handlerStr, Object[] argv) {
    Object res = null;
    Object handler = null;
    try {
      if (handlerStr != null) {
        if (argv == null) {
          handler = Class.forName(handlerStr).getConstructor(new Class[]{Thinlet.class}).newInstance(
              new Object[]{this});
        } else {
          handler = Class.forName(handlerStr).getConstructor(new Class[]{Thinlet.class, Object[].class})
              .newInstance(new Object[]{this, argv});
        }
      }
      if (handler != null) {
        res = parse(compView, handler);
      } else res = parse(compView);
      if (parent != null) {
        if (parent instanceof Thinlet)
          add(res);
        else add(parent, res);
      }
      if (handler != null && handler instanceof Handler) {
        log.debug("Set handler::" + handler.toString());
        m_LastHandler = (Handler) handler;
      }
      return res;
    } catch (Exception ex) {
      log.error("addComponent()", ex);
      return null;
    }
  }//addComponent

  /**
   * Add a Thinlet component from XUL file.
   *
   * @param parent   add the new component to this parent
   * @param compView path to the XUL resource
   * @param handler  handler for the component
   */
  public Object addComponent(Object parent, String compView, Object handler) {
    Object res = null;
    try {
      if (handler != null) {
        res = parse(compView, handler);
      } else res = parse(compView);
      if (parent != null) {
        if (parent instanceof Thinlet)
          add(res);
        else add(parent, res);
      }
      if (handler != null && handler instanceof Handler) {
        log.debug("Set handler::" + handler.toString());
        m_LastHandler = (Handler) handler;
      }
      return res;
    } catch (Exception ex) {
      log.error("addComponent()", ex);
      return null;
    }
  }//addComponent

  public void activate() {
    m_Services = Activator.getServices();
    //check for configuration service

  }//activate

  public void deactivate() {
    log.debug("deactivate()");
    doLogout();
    m_Icons.clear();
    m_Services.deactivate();
    m_Resources = null;
    m_Dialog = null;
    m_ErrorDialog = null;
    m_Services = null;
    if (m_LastHandler != null) {
      m_LastHandler.deinit();
    }
    m_LastHandler = null;
    m_Icons = null;
  }//deactivate

  public static void main(String[] args) {
    //BasicConfigurator.configure();
    //ManagerDesktopImpl md = new ManagerDesktopImpl();
    //new FrameLauncher("Coalevo Management Desktop", md, 400, 400);
    //JOptionPane.showMessageDialog(null, "test", "test",
    //    JOptionPane.ERROR_MESSAGE, new ImageIcon("net/coalevo/manager/resources/images/coalevo.png"));

  }//main

  public Image getIcon(String name) {

    if (m_Icons.containsKey(name)) {
      return (Image) m_Icons.get(name);
    }
    try {
      final URL ir = Activator.getBundleContext().getBundle().getResource(name);
      final InputStream is = ir.openStream();
      byte[] data = new byte[is.available()];
      int len = is.read(data);
      Image i = Toolkit.getDefaultToolkit().createImage(data, 0, len);
      MediaTracker mediatracker = new MediaTracker(this);
      mediatracker.addImage(i, 1);
      try {
        mediatracker.waitForID(1, 5000);
      } catch (InterruptedException ie) {
        log.error("getIcon()",ie);
      }
      m_Icons.put(name, i);
      return i;
    } catch (Exception e) {
      log.error("getIcon()", e);
      return null;
    }
  }//getIcon


  private static final String AUTH_STATUS_LOGGEDIN = "net/coalevo/manager/resources/images/login.png";
  private static final String AUTH_STATUS_LOGGEDOUT = "net/coalevo/manager/resources/images/logout.png";

}//class ManagerDesktopImpl
