/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.manager.impl;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Provides a <tt>FilterInputStream</tt> that allows to follow the
 * loading progress.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ProgressInputStream extends FilterInputStream {

  private ProgressIndicator m_ProgressIndicator;
  private int m_Size;
  private int m_BytesRead = 0;
  private int m_APercent;
  private int m_PercentRead = 0;
  private boolean m_Cancelled = false;

  public ProgressInputStream(InputStream in, ProgressIndicator h, int size) {
    super(in);
    m_ProgressIndicator = h;
    m_Size = size;
    m_APercent = size / 100;
    if (m_APercent == 0) {
      m_APercent = 1;
    }
  }//constructor

  public void cancel(boolean b) {
    m_Cancelled = b;
  }//cancel

  public int read() throws IOException {
    if (m_Cancelled) {
      return -1;
    }
    // System.out.println("read");
    int i = in.read();
    m_BytesRead++;
    //System.out.println(m_BytesRead);
    if (i == -1 && m_BytesRead >= m_Size) {
      m_ProgressIndicator.setProgress(100);
    } else {
      //System.out.println(m_BytesRead / m_APercent);
      if ((m_BytesRead / m_APercent) > m_PercentRead) {
        m_PercentRead++;
        m_ProgressIndicator.setProgress(m_PercentRead);
      }
    }
    return i;
  }//read

  public int read(byte[] data, int off, int length) throws IOException {
    if (m_Cancelled) {
      return -1;
    }
    int i = in.read(data, off, length);
    if (i == -1 && m_BytesRead == m_Size) {
      m_ProgressIndicator.setProgress(100);
    } else {
      m_BytesRead += i;
      m_ProgressIndicator.setProgress(m_BytesRead / m_APercent);
    }
    return i;
  }//read

  public int read(byte[] data) throws IOException {
    return read(data, 0, data.length);
  }//read

}//ProgressInputStream